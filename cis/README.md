# Presentaciones asignaturas

Presentaciones de la carrera de Ingeniería en Sistemas de la Universidad nacional de Loja

## Asignaturas

- Sistemas de Información: [Sistemas de información](sistemas-informacion)
- Ingeniería de software II: [Ingeniería de Software II](ingenieria-software-II)
- Compiladores: [Compiladores](compiladores)
